# Usage: conflate fuuse.py -o result.osm -c preview.json -i out.json --osm osm.cache
# See https://wiki.openstreetmap.org/wiki/OSM_Conflator

# What will be put into "source" tags. Lower case please
source = 'Fuuse'

# A fairly unique id of the dataset to query OSM, used for "ref:mos_parking" tags
# If you omit it, set explicitly "no_dataset_id = True"
#dataset_id = 'mos_parking'
no_dataset_id = True

# Tags for querying with overpass api
query = [('amenity', 'charging_station')]

# Use bbox from dataset points (default). False = query whole world, [minlat, minlon, maxlat, maxlon] to override
bbox = True

# How close OSM point should be to register a match, in meters. Default is 100
max_distance = 200

# Delete objects that match query tags but not dataset? False is the default
delete_unmatched = False

# If set, and delete_unmatched is False, modify tags on unmatched objects instead
# Always used for area features, since these are not deleted
tag_unmatched = None

# A set of authoritative tags to replace on matched objects
master_tags = ('ref')

# Download has to be done via an authenticated POST request in advance
download_url = 'invalid'

# A list of SourcePoint objects. Initialize with (id, lat, lon, {tags}).
def dataset(fileobj):
    import json
    import logging

    # https://github.com/ocpi/ocpi/blob/master/mod_locations.asciidoc#user-content-mod_locations_connectortype_enum
    # https://wiki.openstreetmap.org/wiki/Key:socket
    def connector_type_to_socket(connector_type):
        return {
            'CHADEMO': 'chademo',
            #'CHAOJI': 'FIXME',
            'DOMESTIC_A': 'domestic',  # nothing more specific available
            'DOMESTIC_B': 'domestic',  # nothing more specific available
            'DOMESTIC_C': 'domestic',  # nothing more specific available
            'DOMESTIC_D': 'domestic',  # nothing more specific available
            'DOMESTIC_E': 'typee',
            'DOMESTIC_F': 'schuko',
            'DOMESTIC_G': 'bs1363',
            'DOMESTIC_H': 'domestic',  # nothing more specific available
            'DOMESTIC_I': 'as3112',
            'DOMESTIC_J': 'domestic',  # nothing more specific available
            'DOMESTIC_K': 'domestic',  # nothing more specific available
            'DOMESTIC_L': 'typeL',
            'DOMESTIC_M': 'domestic',  # nothing more specific available
            'DOMESTIC_N': 'domestic',  # nothing more specific available
            'DOMESTIC_O': 'domestic',  # nothing more specific available
            #'GBT_AC': 'FIXME',
            #'GBT_DC': 'FIXME',
            'IEC_60309_2_single_16': 'cee_blue',
            'IEC_60309_2_three_16': 'cee_red_16a',
            'IEC_60309_2_three_32': 'cee_red_32a',
            'IEC_60309_2_three_64': 'cee_red_63a',
            'IEC_62196_T1': 'type1',
            'IEC_62196_T1_COMBO': 'type1_combo',
            'IEC_62196_T2': 'type2',
            'IEC_62196_T2_COMBO': 'type2_combo',
            'IEC_62196_T3A': 'type3',
            'IEC_62196_T3C': 'type3c',
            'NEMA_5_20': 'nema_5_20',
            'NEMA_6_30': 'nema_6_30',
            'NEMA_6_50': 'nema_6_50',
            'NEMA_10_30': 'nema_10_30',
            'NEMA_10_50': 'nema_10_50',
            'NEMA_14_30': 'nema_14_30',
            'NEMA_14_50': 'nema_14_50',
            #'PANTOGRAPH_BOTTOM_UP': 'FIXME',
            #'PANTOGRAPH_TOP_DOWN': 'FIXME',
            'TESLA_R': 'tesla_roadster',
            'TESLA_S': 'tesla_standard',
        }[connector_type]

    source = json.load(fileobj)
    data = []
    operators = {}

    for operator in source['operators']:
        operators[operator['operatorId']] = operator['name']

    for site in source['sites']:
        try:
            network_name = operators[site['operatorID']]
        except KeyError:
            continue

        # Limit to Charge My Street for now
        if site['operatorID'] != 'CmsDemo':
            continue

        for charger in site['charger']:
            lat = charger['latitude']
            lon = charger['longitude']
            charger_id = charger['chargerId']
            ref = charger['physicalReference']

            n_sockets = {}
            socket_power = {}
            socket_voltage = {}
            socket_current = {}

            if charger['availabilityStatus'] != 'AVAILABLE' or \
               not charger['connector']:
                continue

            for connector in charger['connector']:
                if connector['connectorStatus']['statusCode'] != 'Available':
                    continue

                try:
                    socket = connector_type_to_socket(connector['connectorTypeId'])
                except KeyError:
                    continue

                try:
                    n_sockets[socket] = n_sockets[socket] + 1
                except KeyError:
                    n_sockets[socket] = 1
                
                socket_power[socket] = float(connector['ratedOutputkW'])
                socket_voltage[socket] = float(connector['ratedOutputVoltage'])
                socket_current[socket] = float(connector['ratedOutputCurrent'])

            tags = {
                'amenity': 'charging_station',
                'motorcar': 'yes',
                'ref': ref,
                'authentication:app': 'yes',
                'payment:app': 'yes',
                'network': network_name,
            }

            for (socket, count) in n_sockets.items():
                tags['socket:' + socket] = count
            for (socket, output) in socket_power.items():
                if output > 0:
                    tags['socket:' + socket + ':output'] = '{:.0f}'.format(output) + ' kW'
            for (socket, voltage) in socket_voltage.items():
                if voltage > 0:
                    tags['socket:' + socket + ':voltage'] = '{:.0f}'.format(voltage)
            for (socket, current) in socket_current.items():
                if current > 0:
                    tags['socket:' + socket + ':current'] = '{:.0f}'.format(current)

            try:
                lat = float(lat)
                lon = float(lon)
                data.append(SourcePoint(charger_id, lat, lon, tags))
            except Exception as e:
                logging.warning('PROFILE: Failed to parse lat/lon/ref for charger %s: %s', charger_id, str(e))

    return data

