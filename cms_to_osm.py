#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2021 Philip Withnall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import csv
import sys
import xml.etree.cElementTree as ET


# Convert a spreadsheet of EV charge points from ChargeMyStreet into an
# OpenStreetMap XML file ready for checking and importing.
#
# Run as:
# ```
# cms_to_osm.py ./path/to/read.csv ./path/to/write.osm
# ```


def main():
    parser = argparse.ArgumentParser(description="Convert ChargeMyStreet CSV to OSM XML")
    parser.add_argument('input', nargs='?', type=argparse.FileType('r', encoding='utf-8'),
                        default=sys.stdin)
    parser.add_argument('output', nargs='?', type=argparse.FileType('w', encoding='utf-8'),
                        default=sys.stdout)

    args = parser.parse_args()

    # Start building the output
    osm = ET.Element('osm')
    osm.set('version', '0.6')
    osm.set('generator', sys.argv[0])

    # Parse the input
    reader = csv.DictReader(args.input)
    for node_idx, row in enumerate(reader):
        for ref_idx, ref in enumerate(row['Charge Point ID'].split(' ')):
            assert(ref_idx < 100)
            node = ET.SubElement(osm, 'node')
            node.set('id', str(-(node_idx * 100 + ref_idx + 1)))
            node.set('lat', row['Lat'])
            # Shift the longitude by a small amount for each new ref; the
            # charge points are typically installed side-by-side
            node.set('lon', str(float(row['Long']) + 0.00001 * ref_idx))

            tags = {
                'amenity': 'charging_station',
                'authentication:app': 'yes',
                'capacity': '1',
                'fee': 'yes',
                'motorcar': 'yes',
                'network': 'Charge My Street',
                'payment:app': 'yes',
                'ref': ref.strip().strip(','),
            }

            connector_details = row['Connector {} (Type / Speed)'.format(ref_idx + 1)]
            if connector_details == 'Type 2 - 22kW' or connector_details == 'Type 2- 22kW':
                tags.update({
                    'socket:type2': '1',
                    'socket:type2:output': '22kW',
                })
            elif connector_details == 'Type 2 - 7kW':
                tags.update({
                    'socket:type2': '1',
                    'socket:type2:output': '7kW',
                })
            else:
                raise Exception(f'Unrecognised connector details: {connector_details}')

            for key, value in tags.items():
                tag = ET.SubElement(node, 'tag')
                tag.set('k', key)
                tag.set('v', value)

    # Write the XML
    tree = ET.ElementTree(osm)
    tree.write(args.output, encoding='unicode')


if __name__ == "__main__":
    main()

