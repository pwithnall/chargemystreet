#!/bin/sh

login_uri="https://login.fuuse.io/miralisb2c.onmicrosoft.com/b2c_1_fuuseoperatorsignin/oauth2/v2.0/authorize?client_id=6849532b-aaf0-4167-8265-d27ceb37a342&redirect_uri=https%3A%2F%2Fapp.fuuse.io%2Fsignin-oidc&response_type=code%20id_token&scope=openid%20profile%20offline_access%20https%3A%2F%2Fmiralisb2c.onmicrosoft.com%2Fapi%2Fuser_impersonation&response_mode=form_post&nonce=637864058331515851.MjJlN2ZmMzctZGYwOS00MDRiLThmYmUtNzA0Y2VlNzBkMTEzZWJiNjdiMWItNzdiZi00ZDNkLTkwNjAtYjYyMzQ1MTEyY2Yw&state=CfDJ8GAXmqFUp09KosbanGCc51OuKh9XZ1QZnU1_UNd5SneH5_ng4K5smUkmPcxrR-QantnokYi_x4YlcnHJjwysKW5KNW5hLgIpuZwvjijj1Oq3Ly6FKzDZKi1j_Hy1uxkKHOTEmNRir0lyB8QctDj_inJpmowxER8cDtSgqOAsqSizcGyrdSWAJ2OwHC5zRHdHUJraOBI-X8MegXZqpnCDyycgqMYjErZUOPOuB4b8vklkbVlW_VTLdBdNNyxTU9GRqo1JBvWjosKPBUCBBzYxFOfY9-ceh45xNr0tzrBLewyYOcDqB47V4Dv-uNAKOvweYnas9k-6Xn50cAiTkwvPhR8&x-client-SKU=ID_NETSTANDARD2_0&x-client-ver=5.5.0.0"

if [ -z "$1" ]; then
    echo "Authorization header must be provided. Go to ${login_uri} then log in and copy Authorization header from subsequent request to emsp-api.fuuse.io" >/dev/stderr
    exit 1
fi

curl -X POST -H "Content-Type: application/json" -d '{}' "https://emsp-api.fuuse.io/api/Network/network-map" -H  "accept: application/json" -H  "$1" > fuuse-data.json
if [ ! -s fuuse-data.json ]; then
    echo "Download failure — check authentication token?" >/dev/stderr
    exit 1
fi

conflate fuuse.py -o result.osm -c preview.json -i fuuse-data.json

echo "Now open https://geojson.io and check preview.json"
read

echo "Now open JOSM and check and upload result.osm"
